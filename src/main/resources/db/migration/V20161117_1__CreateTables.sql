CREATE TABLE story_point (
  id                                 uuid         PRIMARY KEY,
  author text NOT NULL,
  tags text DEFAULT '',
  title text NOT NULL,
  latitude DECIMAL,
  longitude DECIMAL,
  image_href text,
  audio_href text,
  created_date                        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  last_modified_date                   TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


