package se.svt.shipit2016.kaskad9.war.model.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@EqualsAndHashCode(of = {"id"})
@Data
public class StoryPoint {

    @Id
    @org.hibernate.annotations.Type(type = "pg-uuid")
    private UUID id = UUID.randomUUID();

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @Column
    private String title;

    @Column
    private double latitude;

    @Column
    private double longitude;

    @Column
    private double radius;

    @Column
    private String author;

    @Column
    private String imageHref;

    @Column
    private String audioHref;

    @Column
    private String tags;

}
