package se.svt.shipit2016.kaskad9.war.util;

import se.svt.shipit2016.kaskad9.war.model.entities.StoryPoint;

import lombok.experimental.UtilityClass;
import org.geojson.Feature;
import org.geojson.LngLatAlt;
import org.geojson.Point;

import java.util.Optional;

@UtilityClass
public class GeoUtils {

    private static final String AUTHOR = "author";
    private static final String IMAGE = "image";
    private static final String AUDIO = "audio";
    private static final String TAGS = "tags";

    public Feature toFeature(StoryPoint storyPoint) {
        Feature feature = new Feature();
        feature.setId(storyPoint.getId().toString());
        feature.setGeometry(new Point(storyPoint.getLongitude(), storyPoint.getLatitude()));
        feature.setProperty(AUTHOR, storyPoint.getAuthor());
        feature.setProperty(IMAGE, storyPoint.getImageHref());
        feature.setProperty(AUDIO, storyPoint.getAudioHref());
        feature.setProperty(TAGS, splitTags(storyPoint.getTags()));
        return feature;
    }

    private String[] splitTags(String tagsStr) {
        return Optional.ofNullable(tagsStr)
            .map(str -> str.split(","))
            .orElse(new String[0]);
    }


    public static double distance(Point p1, Point p2) {
        LngLatAlt c1 = p1.getCoordinates();
        LngLatAlt c2 = p2.getCoordinates();
        return distance(c1.getLatitude(), c2.getLatitude(),
            c1.getLongitude(), c2.getLongitude(),
            c1.getAltitude(), c2.getAltitude());
    }

    /*
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    // http://stackoverflow.com/questions/3694380/calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
