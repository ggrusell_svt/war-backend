package se.svt.shipit2016.kaskad9.war.controller;

import se.svt.shipit2016.kaskad9.war.model.entities.StoryPoint;
import se.svt.shipit2016.kaskad9.war.service.StoryPointsService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.geojson.FeatureCollection;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/services")
@RequiredArgsConstructor
public class WarController {

    private final StoryPointsService storyPointsService;

    @GetMapping(value = "/storypoints", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StoryPoint> getStoriesAt(@RequestParam("lat") double latitude,
                                         @RequestParam("long") double longitude,
                                         @RequestParam("dist") double dist) {
        return storyPointsService.getStoryPoints(latitude, longitude, dist);
    }

    @GetMapping(value = "/features", produces = MediaType.APPLICATION_JSON_VALUE)
    public FeatureCollection getFeatures() {
        return storyPointsService.getFeatures();
    }

    @GetMapping(value = "/features/active", produces = MediaType.APPLICATION_JSON_VALUE)
    public FeatureCollection getActiveFeatures(@RequestParam("lat") double latitude,
                                               @RequestParam("long") double longitude) {
        return storyPointsService.getActiveFeatures(latitude, longitude);
    }

    @GetMapping(value = "/health")
    public String getHealth() {
        return "UP";
    }

}

