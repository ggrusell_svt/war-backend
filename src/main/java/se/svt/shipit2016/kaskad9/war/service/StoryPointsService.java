package se.svt.shipit2016.kaskad9.war.service;

import se.svt.shipit2016.kaskad9.war.database.repository.StoryPointRepository;
import se.svt.shipit2016.kaskad9.war.model.entities.StoryPoint;
import se.svt.shipit2016.kaskad9.war.util.GeoUtils;

import lombok.RequiredArgsConstructor;
import org.geojson.FeatureCollection;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static se.svt.shipit2016.kaskad9.war.util.GeoUtils.distance;

@Service
@RequiredArgsConstructor
public class StoryPointsService {

    private final StoryPointRepository storyPointRepository;

    public FeatureCollection getFeatures() {
        FeatureCollection featureCollection = new FeatureCollection();
        storyPointRepository.findAll().stream()
            .map(GeoUtils::toFeature)
            .forEach(featureCollection::add);
        return featureCollection;
    }

    public FeatureCollection getFeaturesAround(double latitude,
                                               double longitude,
                                               double radius) {
        FeatureCollection featureCollection = new FeatureCollection();
        getStoryPoints(latitude, longitude, radius)
            .stream()
            .map(GeoUtils::toFeature)
            .forEach(featureCollection::add);
        return featureCollection;
    }

    public FeatureCollection getActiveFeatures(double latitude,
                                               double longitude) {
        FeatureCollection featureCollection = new FeatureCollection();
        Predicate<StoryPoint> isActive = isActive(latitude, longitude);
        storyPointRepository.findAll().stream()
            .filter(isActive)
            .map(GeoUtils::toFeature)
            .forEach(featureCollection::add);
        return featureCollection;
    }


    public List<StoryPoint> getStoryPoints(double latitude,
                                           double longitude,
                                           double radius) {
        Predicate<StoryPoint> closeEnough = closeEnough(latitude, longitude, radius);
        return storyPointRepository.findAll().stream()
            .filter(closeEnough)
            .collect(toList());
    }

    private Predicate<StoryPoint> closeEnough(double latitude, double longitude, double radius) {
        return storyPoint -> distance(latitude, storyPoint.getLatitude(),
            longitude, storyPoint.getLongitude(), 0d, 0d) < radius;
    }

    private Predicate<StoryPoint> isActive(double latitude, double longitude) {
        return storyPoint -> distance(latitude, storyPoint.getLatitude(),
            longitude, storyPoint.getLongitude(), 0d, 0d) < storyPoint.getRadius();
    }

}
