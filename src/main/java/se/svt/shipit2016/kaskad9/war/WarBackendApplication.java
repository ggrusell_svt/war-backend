package se.svt.shipit2016.kaskad9.war;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(WarBackendApplication.class, args);
    }
}
