package se.svt.shipit2016.kaskad9.war.database.repository;

import se.svt.shipit2016.kaskad9.war.model.entities.StoryPoint;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource
public interface StoryPointRepository extends JpaRepository<StoryPoint, UUID> {


}
